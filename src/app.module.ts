import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          type: 'postgres',
          host: config.get('TYPEORM_HOST') || 'localhost',
          port: parseInt(config.get('TYPEORM_PORT') ?? '5432'),
          username: config.get('TYPEORM_USERNAME') || 'customerapi',
          password: config.get('TYPEORM_PASSWORD') || 'xxx',
          database: 'juzer',
          schema: 'public',
          entities: [`${__dirname}/**/*.entity{.ts,.js}`],
          logging: ['error', 'warn'],
          synchronize: false, // !never enable this!
        };
      },
    }),
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
