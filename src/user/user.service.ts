import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate, Paginated } from 'nestjs-paginate';
import { Repository } from 'typeorm';
import { GetUsersQuery } from './user.dtos';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly repo: Repository<User>,
  ) {}

  get(query: GetUsersQuery): Promise<Paginated<User>> {
    return paginate({ ...query, path: '' }, this.repo, {
      sortableColumns: ['id'],
    });
  }

  getOne(id: string): Promise<User | null> {
    return this.repo.findOneBy({ id });
  }

  create(user: User): Promise<User> {
    return this.repo.save(user);
  }

  async delete(id: string): Promise<void> {
    await this.repo.delete({ id });
  }

  async patch(user: User): Promise<User | null> {
    await this.repo.update({ id: user.id }, user);
    return this.repo.findOneBy({ id: user.id });
  }
}
