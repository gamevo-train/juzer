import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import {
  GetUsersQuery,
  GetUsersResponse,
  NewUserDTO,
  UserDTO,
} from './user.dtos';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly service: UserService) {}

  @Get()
  async get(@Query() query: GetUsersQuery): Promise<GetUsersResponse> {
    const usersRaw = await this.service.get(query);
    const users = usersRaw.data.map((u) => plainToInstance(UserDTO, u));
    return {
      users,
      page: usersRaw.meta.currentPage,
      totalPages: usersRaw.meta.totalPages,
    };
  }

  @Get(':id')
  async getOne(@Param('id') id: string): Promise<UserDTO> {
    const userRaw = await this.service.getOne(id);
    return plainToInstance(UserDTO, userRaw);
  }

  @Post('')
  async create(@Body() newUser: NewUserDTO): Promise<UserDTO> {
    const userRaw = await this.service.create(plainToInstance(User, newUser));
    return plainToInstance(UserDTO, userRaw);
  }

  @Patch('')
  async patch(@Body() newUser: Partial<UserDTO>): Promise<UserDTO> {
    return this.service.patch(plainToInstance(User, newUser));
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    await this.service.delete(id);
  }
}
