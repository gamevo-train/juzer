import { IsEmail, IsOptional, IsUUID, Length } from 'class-validator';

export class NewUserDTO {
  @IsEmail()
  email: string;

  @Length(1, 50)
  firstName: string;

  @Length(1, 50)
  lastName: string;
}

export class UserDTO {
  @IsUUID()
  id: string;
}

export class GetUsersResponse {
  users: UserDTO[];
  page: number;
  totalPages: number;
}

export class GetUsersQuery {
  @IsOptional()
  limit?: number;

  @IsOptional()
  page?: number;
}
