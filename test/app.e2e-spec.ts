import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { User } from './../src/user/user.entity';
import { randomUUID } from 'crypto';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let testUserOne: User;
  let testUserTwo: User;
  let newUserId: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(async () => {
    testUserOne = await User.save({
      email: 'test.one@test.pl',
      firstName: 'test',
      lastName: 'two',
    });
    testUserTwo = await User.save({
      email: 'test.one@test.pl',
      firstName: 'test',
      lastName: 'two',
    });
  });

  afterEach(async () => {
    await User.delete([testUserOne.id, testUserTwo.id, newUserId]);
  });

  describe('USER', () => {
    describe('GET /', () => {
      it('returns all users', async () => {
        const res = await request(app.getHttpServer()).get('/user');
        expect(res.status).toEqual(200);

        const testUsers = res.body.users.filter((u) =>
          [testUserOne.id, testUserTwo.id].includes(u.id),
        );

        expect(testUsers).toHaveLength(2);
        expect(testUsers.find((u) => u.id === testUserOne.id)).toMatchObject({
          id: testUserOne.id,
          email: testUserOne.email,
          firstName: testUserOne.firstName,
          lastName: testUserOne.lastName,
        });
      });

      it('handles paging limit', async () => {
        const res = await request(app.getHttpServer()).get('/user?limit=1');
        expect(res.status).toEqual(200);
        expect(res.body.users).toHaveLength(1);
      });

      it('handles paging limit', async () => {
        const res = await request(app.getHttpServer()).get(
          '/user?limit=1&page=2',
        );
        expect(res.status).toEqual(200);
        expect(res.body.users).toHaveLength(1);
      });
    });

    describe('POST /', () => {
      it('rejects invalid input', async () => {
        const res = await request(app.getHttpServer())
          .post('/user')
          .send({ wrong: 'data' });
        expect(res.status).toEqual(400);
        expect(res.body.message).toHaveLength(
          3,
        ); /** one for each expected property */
      });

      it('allows saving a valid user', async () => {
        const newUser = {
          email: 'roger@waters.com',
          firstName: 'Roger',
          lastName: 'Waters',
        };

        const res = await request(app.getHttpServer())
          .post('/user')
          .send(newUser);

        expect(res.status).toEqual(201);
        const resUser = res.body;
        expect(resUser.id).toBeDefined();
        newUserId = resUser.id;
        const savedUser = await User.findOneBy({ id: resUser.id });
        expect(savedUser.email).toEqual(newUser.email);
        expect(savedUser.firstName).toEqual(newUser.firstName);
        expect(savedUser.lastName).toEqual(newUser.lastName);
      });
    });

    describe('GET /:id', () => {
      it('retuns user by id', async () => {
        const res = await request(app.getHttpServer()).get(
          `/user/${testUserOne.id}`,
        );
        expect(res.status).toEqual(200);
        expect(res.body).toMatchObject({
          id: testUserOne.id,
          email: testUserOne.email,
          firstName: testUserOne.firstName,
          lastName: testUserOne.lastName,
        });
      });

      it('does not return user with wrong id', async () => {
        const res = await request(app.getHttpServer()).get(
          `/user/${randomUUID()}`,
        );
        expect(res.status).toEqual(200);
        expect(res.body).toEqual({});
      });
    });

    describe('PATCH /', () => {
      it('allows updating a user', async () => {
        const newEmail = 'updated.email@wp.pl';
        const res = await request(app.getHttpServer()).patch('/user').send({
          id: testUserOne.id,
          email: newEmail,
        });

        expect(res.status).toEqual(200);
        expect(res.body.email).toEqual(newEmail);

        const savedUser = await User.findOneBy({ id: testUserOne.id });
        expect(savedUser.email).toEqual(newEmail);
        expect(savedUser.firstName).toEqual(testUserOne.firstName);
      });
    });

    describe('DELETE /:id', () => {
      it('removes the user by id', async () => {
        const res = await request(app.getHttpServer()).delete(
          `/user/${testUserOne.id}`,
        );
        expect(res.status).toEqual(200);

        const removedUser = await User.findOneBy({ id: testUserOne.id });
        expect(removedUser).toBeNull();

        const otherUser = await User.findOneBy({ id: testUserTwo.id });
        expect(otherUser.email).toEqual(testUserTwo.email);
      });
    });
  });
});
