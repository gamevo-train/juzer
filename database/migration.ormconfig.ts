import { DataSource } from 'typeorm';

export const connectionSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'secret',
  database: 'juzer',
  entities: ['src/**/*.entity.ts'],
  migrations: ['database/migrations/*.ts', 'migrations/*.ts'],
});
