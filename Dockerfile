# api application dokerfile

FROM node:18.14.1-buster-slim as builder
WORKDIR /home
COPY ./package*.json tsconfig*.json .npmrc ./
RUN npm install
COPY ./src ./src
RUN npm run build:nest

FROM node:18.14.1-buster-slim as runner

ENV NODE_ENV=production

WORKDIR /home/node

COPY --chown=node:node ./package*.json .npmrc ./
RUN npm ci --only=production
COPY --chown=node:node --from=builder /home/output ./output

EXPOSE 8200

RUN apt-get update -y
RUN apt-get install -y dumb-init
RUN apt-get install -y curl

HEALTHCHECK --interval=5s --timeout=3s --start-period=10s --retries=3 \
  CMD curl -f http://127.0.0.1:8200/status || exit 1

USER node

CMD ["dumb-init", "node","output/src/index.js"]
